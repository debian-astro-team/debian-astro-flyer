import popcon
import apt

import wordcloud
import matplotlib.pyplot as plt

astro_tasks = [
    'catalogs',
    'datareduction',
    'development',
    'education',
    'frameworks',
    'gdl',
    'iraf',
    'java',
    'publication',
    'python3',
    'radioastronomy',
    'simulation',
    'tcltk',
    'telescopecontrol',
    'tools',
    'viewers',
    'virtual-observatory',
    ]

other = { 'gnudatalanguage', 'qfits', 'wxastrocapture', 'gcx',
          'ccfits', 'giza', 'plplot'}

renaming = {
    'starjava-topcat': 'topcat',
    'starjava-ttools': 'stilts',
    'starjava-table': 'stil',
    'starjava-votable': 'starjava',
    'starjava-vo': 'starjava',
    'starjava-fits': 'starjava',
    'starjava-array': 'starjava',
    'starjava-util': 'starjava',
    'starjava-task': 'starjava',
    'starjava-connect': 'starjava',
    'starjava-registry': 'starjava',
    'starjava-pal': 'starjava',
    'starjava-cdf': 'starjava',
    'python-astropy': 'astropy',
    'python-astropy-affiliated': 'astropy-affiliated',
    'python-asdf': 'asdf',
    'python-pyds9': 'pyds9',
    'tcl-fitstcl': 'fitstcl',
    'ftools-fv': 'fv',
    'sextractor': 'source-exctractor',
}

multiplier = {
    'astrometry.net': 0.5,
    'stellarium': 0.5,
    'iraf': 2,
    'topcat': 2,
    'stil': 2,
    'stilts': 2,
    'aladin': 2,
    'gammapy': 2,
    'jcdf': 2,
    'pyraf': 2,
    'radio-beam': 2,
    'skyview': 2,
    'adql': 2,
    'astrodendro': 2,
    'aravis': 2,
    'astropy-healpix': 2,
    'astropy-regions': 2,
    'drms': 2,
    'eag-healpix': 2,
    'gwcs': 2,
    'imexam': 2,
    'inhomog': 2,
    'iraf-fitsutil': 2,
    'iraf-mscred': 2,
    'iraf-rvsao': 2,
    'iraf-sptable': 2,
    'libnexstar': 2,
    'linguider': 2,
    'mpgrafic': 2,
    'ser-player': 2,
    'voro++': 2,
}

packages = {}
c = apt.Cache()
for t in astro_tasks:
    print(t)
    tt = {}
    packages[t] = tt
    debian_task = 'astro-' + t
    if debian_task not in c:
        print("{} not in cache".format(debian_task))
        continue
    p = c.get(debian_task).versions[-1]
    for r in p.recommends:
        name = r[0].name
        if name not in c:
            continue
        pt = c[name].versions[-1]
        srcname = pt.source_name
        maint = pt.record['maintainer']
        if 'astro' in maint or srcname in other:
            pp = popcon.packages([name])
            srcname = renaming.get(srcname, srcname)
            tt.setdefault(srcname, 0)
#            tt[srcname] = max(pp[name], tt[srcname])
            tt[srcname] += pp[name] * multiplier.get(srcname, 1)


frequencies = {}
tasks = {} # later to be used for color
for t, p in packages.items():
    for pp in p:
        frequencies.setdefault(pp, 0)
        frequencies[pp] += p[pp]
        tasks[pp] = t

w = wordcloud.WordCloud(max_font_size=200,
                        background_color="#fff0",
                        width=860,
                        height=1900)
w = w.generate_from_frequencies(frequencies)
w.to_file("packagecloud.png")
#plt.figure()
#plt.imshow(w, interpolation="bilinear")
#plt.axis("off")
#plt.show()
